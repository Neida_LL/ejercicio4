import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  anioA!:Date;
  dia!:number;
  mes!:number;
  anio!:number;
  constructor() { 
    this.anioA=new Date();
    this.dia=this.anioA.getDate();
    this.mes=this.anioA.getMonth();
    this.anio=this.anioA.getFullYear();
  }
  ngOnInit(): void {
  }

}
